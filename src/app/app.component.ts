import {Component, OnInit, DoCheck} from '@angular/core';
import { Router } from '@angular/router';
import {DataService} from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, DoCheck {
  path = window.location.pathname;
  documents = [];

  constructor(private router: Router, private documentsService: DataService) { }

  goToActivityList() {
    this.router.navigate(['/activity-list']);
  }
  goToAlerts() {
    this.router.navigate(['/alerts']);
  }
  goToRiskMatch() {
    this.router.navigate(['/risk-match']);
  }
  goToProfile() {
    this.router.navigate(['/profile']);
  }

  ngDoCheck(): void {
    this.path = window.location.pathname;
  }

  ngOnInit() {
    this.documents = this.documentsService.getData();
  }
}
