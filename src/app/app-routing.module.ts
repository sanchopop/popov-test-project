import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AlertsComponent} from './components/alerts/alerts.component';
import {RiskMatchComponent} from './components/risk-match/risk-match.component';
import {ProfileComponent} from './components/profile/profile.component';

const routes: Routes = [
  { path: '', redirectTo: 'activity-list', pathMatch: 'full' },
  { path: 'alerts', component: AlertsComponent },
  { path: 'risk-match', component: RiskMatchComponent },
  { path: 'profile', component: ProfileComponent },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
