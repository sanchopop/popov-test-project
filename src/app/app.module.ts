import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {RouterModule} from '@angular/router';
import {ActivityListComponent} from './components/activity-list/activity-list.component';
import {AlertsComponent} from './components/alerts/alerts.component';
import {ProfileComponent} from './components/profile/profile.component';
import {RiskMatchComponent} from './components/risk-match/risk-match.component';
import {ActivityListModule} from './components/activity-list/activity-list.module';
import {RiskDataComponent} from './components/activity-list/components/risk-data/risk-data.component';
import {DetailedInfoModule} from './components/activity-list/components/detailed-info/detailed-info.module';
import {DataService} from './services/data.service';

@NgModule({
  declarations: [
    AppComponent,
    ActivityListComponent,
    AlertsComponent,
    ProfileComponent,
    RiskMatchComponent,
    RiskDataComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    ActivityListModule,
    DetailedInfoModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
