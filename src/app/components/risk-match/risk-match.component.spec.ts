import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskMatchComponent } from './risk-match.component';

describe('RiskMatchComponent', () => {
  let component: RiskMatchComponent;
  let fixture: ComponentFixture<RiskMatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskMatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskMatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
