import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ActivityListComponent} from './activity-list.component';
import {DetailedInfoComponent} from './components/detailed-info/detailed-info.component';

const routes: Routes = [
  { path: 'activity-list',
    component: ActivityListComponent,
    children: [
      { path: ':id', component: DetailedInfoComponent  }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivityListRoutingModule { }
