import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivityListRoutingModule } from './activity-list-routing.module';
import { CheckDataComponent } from './components/check-data/check-data.component';
import { FollowersComponent } from './components/followers/followers.component';
import { ShareComponent } from './components/share/share.component';

@NgModule({
  declarations: [CheckDataComponent, FollowersComponent, ShareComponent],
  imports: [
    CommonModule,
    ActivityListRoutingModule
  ]
})
export class ActivityListModule { }
