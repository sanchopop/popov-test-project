import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {DataService} from '../../services/data.service';

@Component({
  selector: 'app-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.scss']
})
export class ActivityListComponent implements OnInit {
  tableHeaders = [
    {
      text: 'Status',
      class: 'status',
      iconClass: 'fas fa-question-circle',
    },
    {
      text: 'Document Title',
      class: 'document-title',
      sortField: 'documentTitle',
      filter: true,
      isActive: false,
      isAscending: false,
      isDescending: false
    },
    {
      text: 'Check Date',
      class: 'check-date',
      sortField: 'checkDate',
      filter: true,
      isActive: false,
      isAscending: false,
      isDescending: false
    },
    {
      text: 'Inception Date',
      class: 'inception-date',
      sortField: 'inceptionDate',
      filter: true,
      isActive: false,
      isAscending: false,
      isDescending: false
    },
    {
      text: 'Market',
      class: 'market',
      sortField: 'market',
      filter: true,
      isActive: false,
      isAscending: false,
      isDescending: false
    },
    {
      text: 'Class of Business',
      class: 'class-of-business',
      sortField: 'classOfBusiness',
      filter: true,
      isActive: false,
      isAscending: false,
      isDescending: false
    },
    {
      text: 'Exposure Country',
      class: 'exposure-country',
      sortField: 'exposureCountry',
      filter: true,
      isActive: false,
      isAscending: false,
      isDescending: false
    },
    {
      text: 'Tax Country',
      class: 'tax-country',
      sortField: 'taxCountry',
      filter: true,
      isActive: false,
      isAscending: false,
      isDescending: false
    },
    {
      text: 'Broker',
      class: 'broker',
      sortField: 'broker',
      filter: true,
      isActive: false,
      isAscending: false,
      isDescending: false
    }
  ];
  filteredDocuments = [];
  path = window.location.pathname;

  constructor(private router: Router, private documentsService: DataService) {}

  sort(value, sortField) {
    if (value === 'ascending') {
      this.tableHeaders.forEach(item => {
        if (item.sortField === sortField) {
          item.isActive = true;
          item.isAscending = true;
          item.isDescending = false;
        } else {
          item.isActive = false;
          item.isAscending = false;
          item.isDescending = false;
        }
      });
      this.filteredDocuments.sort((a, b) => {
        if (a[sortField].toLowerCase() < b[sortField].toLowerCase()) { return -1; }
        else if (a[sortField].toLowerCase() > b[sortField].toLowerCase()) { return 1; }
        return 0;
      });
    } else {
      this.tableHeaders.forEach(item => {
        if (item.sortField === sortField) {
          item.isActive = true;
          item.isDescending = true;
          item.isAscending = false;
        } else {
          item.isActive = false;
          item.isAscending = false;
          item.isDescending = false;
        }
      });
      this.filteredDocuments.sort((a, b) => {
        if (a[sortField].toLowerCase() > b[sortField].toLowerCase()) { return -1; }
        else if (a[sortField].toLowerCase() < b[sortField].toLowerCase()) { return 1; }
        return 0;
      });
    }
  }

  goToRiskData(id) {
    this.router.navigate([`activity-list/${id}/risk-data/`]);
  }

  ngOnInit() {
    this.filteredDocuments = [... this.documentsService.getData()];
  }

}
