import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DetailedInfoComponent} from './detailed-info.component';
import {RiskDataComponent} from '../risk-data/risk-data.component';
import {CheckDataComponent} from '../check-data/check-data.component';
import {FollowersComponent} from '../followers/followers.component';
import {ShareComponent} from '../share/share.component';

const routes: Routes = [
  {
    path: 'activity-list/:id',
    component: DetailedInfoComponent,
    children: [
      {
        path: 'risk-data', component: RiskDataComponent
      },
      {
        path: 'check-data', component: CheckDataComponent
      },
      {
        path: 'followers', component: FollowersComponent
      },
      {
        path: 'share', component: ShareComponent

      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailedInfoRoutingModule { }
