import {Component, DoCheck, OnInit} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-detailed-info',
  templateUrl: './detailed-info.component.html',
  styleUrls: ['./detailed-info.component.scss']
})
export class DetailedInfoComponent implements OnInit, DoCheck {
  id = 0;
  path = '';
  pathArr = [];
  riskDataPath = '';
  checkDataPath = '';
  followersPath = '';
  sharePath = '';

  constructor(private router: Router) {}

  checkPath() {
    this.pathArr = window.location.pathname.split('/');
    this.id = this.pathArr[2];
    this.path = this.pathArr[3];
    this.riskDataPath = 'risk-data';
    this.checkDataPath = 'check-data';
    this.followersPath = 'followers';
    this.sharePath = 'share';
  }

  goToRiskData() {
    this.checkPath();
    this.router.navigate([`activity-list/${this.id}/risk-data`]);
  }
  goToCheckData() {
    this.checkPath();
    this.router.navigate([`activity-list/${this.id}/check-data`]);
  }
  goToFollowers() {
    this.checkPath();
    this.router.navigate([`activity-list/${this.id}/followers`]);
  }
  goToShare() {
    this.checkPath();
    this.router.navigate([`activity-list/${this.id}/share`]);
  }

  ngDoCheck(): void {
    this.checkPath();
  }

  ngOnInit() {

  }

}
