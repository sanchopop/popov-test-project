import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskDataComponent } from './risk-data.component';

describe('RiskDataComponent', () => {
  let component: RiskDataComponent;
  let fixture: ComponentFixture<RiskDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
